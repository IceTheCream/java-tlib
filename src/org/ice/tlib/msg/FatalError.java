package org.ice.tlib.msg;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class FatalError extends JDialog {

	/**
	 *
	 */
	private static final long serialVersionUID = -5283589146919490204L;
	JTextArea textArea;

	public int showMessage(String Msg) {
		textArea.setText(Msg);
		extracted();
		return 0;
	}

	private void extracted() {
		setVisible(true);
	}

	public FatalError() {
		setBounds(100, 100, 450, 300);

		getContentPane().setLayout(new BorderLayout(0, 0));
		{

			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.SOUTH);
			{

				JButton btnNewButton = new JButton("        Ок        ");
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						// тут реакция на кнопку тупо закрыть окно.
						setVisible(false);
					}
				});
				panel.add(btnNewButton);
			}
		}
		{
			textArea = new JTextArea();
			textArea.setEditable(false);
			textArea.setFont(new Font("DejaVu Sans", Font.PLAIN, 14));
			getContentPane().add(textArea, BorderLayout.CENTER);
		}
	}

}
