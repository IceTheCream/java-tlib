package org.ice.tlib.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.DriverManager;

import org.ice.tlib.ui.Const;

public class DataBase  {

	private String Driver="";
	private String User="";
	private String Password="";
	private String Encoding=""; 
	private String DBUrl="";
	private String DBPath="";
	private Connection Conn=null;


	
	//***** ���������� ��������� Connection, ���������� � ��	
	public Connection getConnection() {
		return Conn;
	}

	
	//***** ���������� ���� � ��
	public String getDBPath() {
		return DBPath;		
	}
	
	
	//***** ������������� ���� � ��
	public void setDBPath(String aDBPath) {
		DBPath=aDBPath;
	}
	
	
	//***** ���������� URI ��
	public String getDBUrl() {
		return DBUrl;
	}

	
	//***** ������������� URI ��
	public void setDBUrl(String aDBUrl) {
		DBUrl=aDBUrl;
	}


	//***** ���������� URI ��������
	public String getDriver() {
		return Driver;	
	}

	
	//***** ������������� URI ��������
	public void setDriver(String aDriver) {
		Driver=aDriver;
	}

	
	//***** ���������� ��������� ��
	public String getEncoding() {
		return Encoding;
	}

	
	//***** ������������� ��������� ��
	public void setEncoding(String aEncoding) {
		Encoding=aEncoding;
	}

	
	//***** ���������� ������ ������������ ��
	public String getPassword() {
		return Password;		
	}
	
	
	//***** ������������� ������ ������������ ��
	public void setPassword(String aPassword) {
		Password=aPassword;		
	}
	
		
	//***** ���������� ������������ ��
	public String getUser() {
		return User;
	}
	

	//***** ������������� ������������ ��
	public void setUser(String aUser) {
		User=aUser;
	}

	
	//***** ������������� ���������� � ��	
	protected void connectDataBase() throws SQLException {
		Properties props=new Properties();
		
		props.setProperty("user", User);
		props.setProperty("password", Password);
		props.setProperty("encoding", Encoding);
	
		Conn=DriverManager.getConnection(DBUrl+DBPath, props);
	}


	//***** ��������� ���������� � ��
	public int open() {
		if(!Driver.isEmpty()) {
			try {
				useDriver();
			} catch (InstantiationException e) {
				System.out.print("** Err. Database.Open.useDriver : Exception occured. "+e.getMessage());
				return Const.CT_INVALID;
			} catch (IllegalAccessException e) {
				System.out.print("** Err. Database.Open.useDriver : Exception occured."+e.getMessage());
				return Const.CT_INVALID;
			} catch (ClassNotFoundException e) {
				System.out.print("** Err. Database.Open.useDriver : Exception occured."+e.getMessage());
				return Const.CT_INVALID;
			}
			if(!(User.isEmpty() || Password.isEmpty() || Encoding.isEmpty())) {
				try {
					connectDataBase();
				} catch (SQLException e) {
					System.out.print("** Err. Database.Open.connectDataBase : Exception occured."+e.getMessage());
					return Const.CT_INVALID;
				}
				return Const.CT_VALID;
			} else {
				System.out.print("** Err. Database user or password or encoding not specified.");
				return Const.CT_INVALID;
			}
		} else {
			System.out.print("** Err. Database driver not specified.");
			return Const.CT_INVALID;
		}
	}


	//***** ��������� ��
	public int close() {
		try {
			Conn.close();
		} catch (SQLException e) {
			System.out.print("** Err. Database:close:conn.close : Exception occured."+e.getMessage());
		}
		return 0;
	}


	//***** ���������� � ���������
	protected void useDriver()  throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Class.forName(Driver).newInstance();
	}

}
