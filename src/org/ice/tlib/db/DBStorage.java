package org.ice.tlib.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.ice.tlib.ui.AbstractStorage;

public class DBStorage extends AbstractStorage {
	
	private ArrayList<ArrayList<Object>> dataArray=null;
	private ArrayList<FieldDef> fieldDefs=null;
	private Query query=null;
	
	private Integer[] fieldIndexes;
	private int visibleColumns=0;
	private int totalColumns=0;
	private int totalRows=0;
	private int keyFieldIndex=0;
	private String[] fieldNames;
	
	//equals( ), hashCode( ), toString( ), clone( ) (���������� Cloneable) 
	//� ���������� Comparable � Serializable. 
	
	
	//***** �����������
	public DBStorage () {
		
		dataArray=new ArrayList<ArrayList<Object>>();
		fieldDefs=new ArrayList<FieldDef>();		
	}

	
	//***** ���������� �������� ������ � ������������ rowIndex � colIndex
	public Object getCell(int rowIndex, int colIndex) {
		
		ArrayList<Object> row=dataArray.get(rowIndex);
		int idx=fieldIndexes[colIndex];
		return row.get(idx);
	}
		
	
	//***** ���������� �������� ������ �� ����� ���� ��
	public String getCellByName(int rowIndex, String fieldName) {

		int idx=0;
		for(String name:fieldNames) {
			if(name.equalsIgnoreCase(fieldName)) {
				ArrayList<Object> row=dataArray.get(rowIndex);
				return row.get(idx).toString();
			}
			idx++;
		}
		return "";
	}

	
	//***** ���������� ����� ������� ������
	public Class<?> getColumnClass(int colIndex) throws DataBaseException {
		
		FieldDef def=null;
		if(fieldDefs==null ) {

			throw new DataBaseException("\n !!! Query:getColumnClass:[Types] - Type array not yet allocated, cannot get column type");		
		} else {
			
			if(fieldDefs.size()>totalColumns) {

				throw new DataBaseException("\n !!! Query:getColumnClass:[Types] -  aColIndex is out of bounds "+totalColumns+" - "+colIndex);		
			} else {

				def=fieldDefs.get(colIndex);
				if(def!=null) {

					switch(def.getType()) {
					case ftINT:
						return int.class;
					case ftSTRING:
						return String.class;
					case ftDATE:
						return Date.class;
					default:
						return String.class;
					}
				} else {
					
					return String.class;
				}
			}
		}
	}

	
	//***** ���������� ���� ������ ������
	public ArrayList<ArrayList<Object>> getDataArray() {
		return dataArray;
	}

	
	//***** ������������� ������ ������
	public void setDataArray(ArrayList<ArrayList<Object>> dataArray) {
		this.dataArray = dataArray;
	}


	//***** ���������� �������� ��������� ����.
	public Integer getID(int rowIndex) {

		if(rowIndex<totalRows) {

			ArrayList<Object> row=dataArray.get(rowIndex);
			Object obj=row.get(keyFieldIndex);
			String cell=obj.toString(); 
			return Integer.parseInt(cell); 
		}
		return -1;
	}
	
	
	//***** ���������� ������� Query, ��������������� � ����������
	public Query getQuery() {

		return query;
	}

	
	//***** ������������� ���������� query
	public void setQuery(Query query) {

		this.query=query;
	}
	

	//***** ���������� ��������� �������
	public String getTitle(int colIndex) {

		FieldDef def=fieldDefs.get(colIndex);
		return def.getTitle();
	}

	
	//***** ���������� ����� ���������� �������
	public int getTotalColumns() {
		
		return totalColumns;
	}


	//***** ������������� ����� ���������� �������
	public void setTotalColumns(int totalColumns) {

		this.totalColumns = totalColumns;
	}


	//***** ���������� ����� ���������� �����
	public int getTotalRows() {

		return totalRows;
	}


	//***** ������������� ����� ���������� �����
	public void setTotalRows(int totalRows) {

		this.totalRows = totalRows;
	}

	
	//***** ���������� ���������� ������������ ��������
	public int getVisibleColumns() { 

		return visibleColumns;
	}

	
	//***** ������������� ���������� ������������ ��������
	public void setVisibleColumns(int visibleColumns) {

		this.visibleColumns=visibleColumns;
	}
	

	//***** ������������� ������ ��������� ����.
	public void setKeyFieldIndex(int keyFieldIndex) {

		this.keyFieldIndex=keyFieldIndex;
	}
	
	
	//***** ��������� �������� ���� � ������ ��������
	public void addFieldDef(FieldDef fieldDef) {

		fieldDefs.add(fieldDef);
	}

	
	//***** ����������� ������ �����
	private void buildFieldIndexes(String[] fields) {
		
 		fieldIndexes=new Integer[fields.length];
		FieldDef def=null;
		int fieldNum=0;

		//***** ���������� ����� ����� �� ��
		for(int i = 0;i<fields.length;i++) {  

			//***** ���������� �������� ����������� �����
			for(int j=0;j<fieldDefs.size();j++) {

				def=fieldDefs.get(j);
				//***** ���� ����� ��������� - �� ����� ����������� ����� ����.
				if(fields[i].equalsIgnoreCase(def.getName())) {

					//***** ���� ��� ���� ������ ���� ������ � ������� - 
					if(def.getVisible()) {
						
						//***** ��������� ��� � ������� ���� �����
						//fieldIndexes[fieldNum]=j;
						//fieldIndexes[i]=j;
						fieldIndexes[j]=i;
						fieldNum++;
					}
					break;
				}
			}	
	    }
		visibleColumns=fieldNum;
	}


	//***** ��������� ������ �� Query
	public int loadData() throws SQLException {

		int rows=0;
		totalColumns=query.getTotalColumns();
		ArrayList<Object> row=null;
		fieldNames=query.getFieldList();
		buildFieldIndexes(fieldNames);
		row=query.getRow();
		while(row!=null) {
  
			dataArray.add(row);
    		row=query.getRow();
    		rows++;
        }
		totalRows=rows;
		return rows;
	}

	
	//***** ��������� ����� ������ 
	public int reloadData() throws SQLException {

		int rows=0;
		ArrayList<Object> row=null;
		dataArray.clear();
		row=query.getRow();
		while(row!=null) {
 
			dataArray.add(row);
    		row=query.getRow();
    		rows++;
        }
		totalRows=rows;
		return rows;
	}
	
}
