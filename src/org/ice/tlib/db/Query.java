package org.ice.tlib.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

import java.util.ArrayList;

import org.ice.tlib.ui.Const;



public class Query extends Object{

	public  static final char PARAMDELIMITER='%';

	private ResultSet resultSet=null;
	private Statement statem=null;
	private Connection connect=null;
	private String[] fieldList=null;

	private String sqlText=null;
	private String resultSQL;
	private ArrayList<Parameter> params;

	private int State=Const.CT_CLOSED;
	private int totalColumns=0;
	
	private boolean debug=false;
	

	//***** �����������
 	public Query(Connection conn) {

 		connect=conn;
		params=new ArrayList<Parameter>();
	}

	
	//***** ���������� ������ �����
	public String[] getFieldList() {

		return fieldList;
	}

	
	//***** ������������� ������ �����
	public void setFieldList(String[] fields) {

		fieldList = fields;
	}
	

	//***** ���������� ����� �������
	public String getSQL() {

		return sqlText;
	}

	
	//***** �������� � Query ����� �������
	public void setSQL(String sql) throws DataBaseException {

		if(State==Const.CT_CLOSED) {

			sqlText=sql;
		} else {

			throw new DataBaseException("\n !!! Query.setSQL: Cannot assign SQL - Query is open already \n");
		}
	}
	
	
	//***** ���������� ResultSet � ������� ������
	public ResultSet getResultSet() {

		return resultSet;	
	}

	
	//***** ���������� ������ ������
	public ArrayList<Object> getRow() throws SQLException {

		ArrayList<Object> row=null;
 		if(resultSet.next()) {

 			row=new ArrayList<Object>(totalColumns);
	        for (int i = 1; i <= totalColumns; i++) {

	        	row.add(resultSet.getObject(i));
	        }
 		}
        return row;
	}

	
	//***** ���������� ����� ���������� ��������
	public int getTotalColumns() {

		return totalColumns;
	}
	
	
	//***** ��������� SQL ������ � �������� ���������.
	public void open(String sql) throws SQLException, DataBaseException {
	
		setSQL(sql);
		if(prepare(sql)) {

			statem=connect.createStatement();
			//Boolean hasresultset;
			if(debug) {

				System.out.println("\n +++SQL:"+resultSQL);
			}
	 		resultSet=statem.executeQuery(resultSQL);
	 		State=Const.CT_OPENED;
	 		retrieveColumns();
	 		retrivefieldList();
		} else {

			if(State!=Const.CT_CLOSED) {

				throw new DataBaseException("\n !!! Query:open Cannot open Query - Query already is open \n");
			}	
		 	
			if(sqlText.isEmpty()) {

				throw new DataBaseException("\n !!! Query:open Cannot open Query - SQL string is empty \n");
			}
		}
	}

	//***** ��������� ��������� ������� � �������� ����� buidSQL, ���� �� ������
	public boolean prepare(String sql) throws DataBaseException {

		setSQL(sql);
		if(State==Const.CT_CLOSED) {
		 	
			if(!sqlText.isEmpty()) {

				if(!params.isEmpty()) {
				
		 			buildSQL();
		 			return true;
				}
		 	} 
		}		
		return false;
	}
	
	
	//***** ��������� ������ ���� INSERT, UPDATE, DELETE � ������
	public void exec(String sql) throws SQLException, DataBaseException {

		setSQL(sql);
		if(prepare(sqlText)) {

			statem=connect.createStatement();
			if(debug) {

				System.out.println("\n +++SQL:"+resultSQL);
			}
		 	statem.executeUpdate(resultSQL);
			
		} else {

			if(State!=Const.CT_CLOSED) {

				throw new DataBaseException("\n !!! Query:exec Cannot execute Query - Query is open \n");
			}	
		 	
			if(sqlText.isEmpty()) {

				throw new DataBaseException("\n !!! Query:exec Cannot execute Query - SQL string is empty \n");
			}
			
		}
	}

	
	//***** ��������� ������ ���� INSERT, UPDATE, DELETE � ������
	public void exec() throws SQLException, DataBaseException {

		if(prepare(sqlText)) {

			statem=connect.createStatement();
			if(debug) {

				System.out.println("\n +++SQL:"+resultSQL);
			}
		 	statem.executeUpdate(resultSQL);
			
		} else {

			if(State!=Const.CT_CLOSED) {

				throw new DataBaseException("\n !!! Query:exec Cannot execute Query - Query is open \n");
			}	
		 	
			if(sqlText.isEmpty()) {

				throw new DataBaseException("\n !!! Query:exec Cannot execute Query - SQL string is empty \n");
			}
			
		}
	}

	
	//***** ���������  �������
	public void close() throws SQLException, DataBaseException {
	
		if(State==Const.CT_OPENED) {
		
			resultSet.close();
			statem.close();
			params.clear();
			State=Const.CT_CLOSED;
		} else {
		
			throw new DataBaseException("\n !!! Query:close Cannot close Query - Query is not open \n");
		}			
	}
	
	
	//***** ������ ������ ������� ������ � ���� �� �����������
	public void  refresh() throws SQLException, DataBaseException {
	
		if(State==Const.CT_OPENED) {
	
			resultSet.close();
			statem.close();
			State=Const.CT_CLOSED;
		} else {
			
			throw new DataBaseException("\n !!! Query:close Cannot close Query - Query is not open \n");
		}			
		open(getSQL());
	}

	
	//***** �������� �� �� ������ �����.
	protected void retrivefieldList() throws SQLException {

 		fieldList= new String[totalColumns];
		ResultSetMetaData rsmd = resultSet.getMetaData();
		for(int i = 0; i <totalColumns; i++) {  

			fieldList[i]= rsmd.getColumnLabel(i+1);
	    }
	}

	
	//***** �������� �� �� ���������� � ���������� �������� � �������
	protected void retrieveColumns() throws SQLException {
		
		ResultSetMetaData rsmd = null;
		rsmd = resultSet.getMetaData();
		totalColumns=rsmd.getColumnCount();
	}


	//***** ��������� � ������ ������� (�������� ��?)
	public void firstRecord() throws SQLException {
		resultSet.first();
	}
	
	
	public void addParameter(String name, String value) {
		
		Parameter param=new Parameter(name,value);
		params.add(param);
	}

	
	protected void buildSQL() throws DataBaseException { //XXX
		
		int posBegin=0,posEnd=-1;
		String partsSQL[]=new String[params.size()+1];
		String paramNames[]=new String[params.size()];
		String s;
		resultSQL="";
		
		//***** ��������� SQL �� �������, ������� ���������.
		for(int i=0;i<params.size();i++) {

			// ��������� �� ����� �� ������� ��������� posEnd+1
			posBegin=sqlText.indexOf(PARAMDELIMITER,posEnd+1);

			// ��������� �� -1
			partsSQL[i]=sqlText.substring(posEnd+1, posBegin);
			posEnd=sqlText.indexOf(PARAMDELIMITER,posBegin+1);

			// ��������� �� -1
			paramNames[i]=sqlText.substring(posBegin+1,posEnd);
		}
		partsSQL[params.size()]=sqlText.substring(posEnd+1);
		
		//***** �������� SQL, ���������� �������� ����������
		for(int i=0;i<params.size();i++) {

			for(int j=0;j<params.size();j++) {

				s=params.get(j).getName();
				if(s.equalsIgnoreCase(paramNames[i])) {

					resultSQL+=partsSQL[i]+params.get(j).getValue();
					break;
				}
				//���������, ����� ��...
			}
		}	
		resultSQL+=partsSQL[params.size()];
	}
	

	public boolean isDebug() {

		return debug;
	}
	

	public void setDebug(boolean debug) {

		this.debug = debug;
	}
	
	
}
