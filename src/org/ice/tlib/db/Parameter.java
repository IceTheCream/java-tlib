package org.ice.tlib.db;

public class Parameter extends Object {

	private String paramName;
	private String paramValue;
	
	
	Parameter(String paramName, String paramValue) {

		this.paramName=paramName;
		this.paramValue=paramValue;
	}
	
	
	public String getName() {

		return paramName;
	}
	
	
	public void setName(String paramName) {

		this.paramName = paramName;
	}
	
	
	public String getValue() {

		return paramValue;
	}
	
	
	public void setValue(String paramValue) {
				
		this.paramValue = paramValue;
	}
	
	

}
