package org.ice.tlib.db;


public class FieldDef extends Object{

	public enum FieldTypes {
		ftINT,
		ftSTRING,
		ftDATE,
		ftUNDEF
	}

	private String fieldName=null;
	private String fieldTitle=null;
	private FieldTypes fieldType=FieldTypes.ftUNDEF;
	boolean fieldVisible=false;
	
	
	//***** �����������, ���� ����
	public FieldDef(String fieldName, String fieldTitle, FieldTypes fieldType, boolean fieldVisible) {
		
		this.fieldName=fieldName;
		this.fieldTitle=fieldTitle;
		this.fieldType=fieldType;
		this.fieldVisible=fieldVisible;
	}

	
	public String getName() {

		return fieldName;
	}

	
	public void setName(String fieldName) {

		this.fieldName = fieldName;
	}
	
	
	public String getTitle() {

		return fieldTitle;
	}
	
	
	public void setTitle(String fieldTitle) {

		this.fieldTitle = fieldTitle;
	}

	
	public FieldTypes getType() {

		return fieldType;
	}

	
	public void setType(FieldTypes fieldType) {

		this.fieldType = fieldType;
	}

	
	public boolean getVisible() {

		return fieldVisible;
	}
	
	
	public void setVisible(boolean aVisible) {

		this.fieldVisible=aVisible;
	}

	
	public boolean equals(FieldDef def) {

		return  def.getName()==fieldName && 
				def.getTitle()==fieldTitle &&
				def.getType()==fieldType &&
				def.getVisible()==fieldVisible;
		}

	
	
}