package org.ice.tlib.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JFrame;

public class EasyConfig {

	public enum States {
		stCLOSED,
		stOPENFORREAD,
		stOPENFORWRITE
	}

	
	String fileName=null;
	Properties configProp=null;
	States classState=States.stCLOSED;
	FileOutputStream outStream=null;
	FileInputStream inStream=null;
	
	
	public EasyConfig(String fileName) {
		
		super();
		this.fileName=fileName;
		configProp=new Properties();
	}
	
	
	protected void close() throws IOException {
		
		if(classState!=States.stCLOSED) {
			
			inStream.close();
			classState=States.stCLOSED;
		}
	}
	
	
	protected void openForWriting() throws IOException {
		
		if(classState==States.stOPENFORREAD) {
			
			close();
		}
		
		if(classState==States.stCLOSED) {

			outStream=new FileOutputStream(fileName);
			classState=States.stOPENFORWRITE;
		}
	}

	
	protected void openForReading() throws IOException, ConfigException {
		
		if(classState==States.stOPENFORWRITE) {

			close();
		}
		
		if(classState==States.stCLOSED) {
			
			if(new File(fileName).exists()) {
				
				inStream=new FileInputStream(fileName);
				classState=States.stOPENFORREAD;
			} else {
				
				throw new ConfigException("\n !!! EasyConfig:openForReading:[inStream] - file "+fileName+" not found");
			}
		}
	}

	
	public void writeIntValue(String keyName, String valueName, int value) throws IOException {
		
		openForWriting();
	    configProp.setProperty(keyName+valueName, ((Integer)value).toString());	
	}
	
	
	public int readIntValue(String keyName, String valueName) throws IOException {
		
		String value=null;
		openForWriting();
	    value=configProp.getProperty(keyName+valueName);
	    return Integer.parseInt(value);
	}
	
	
	public int readIntValue(String keyName, String valueName, int defaultValue) throws IOException {
		
		String value=null;
		openForWriting();
	    value=configProp.getProperty(keyName+valueName,((Integer)defaultValue).toString());
	    return Integer.parseInt(value);
	}

	
	public void writeStringValue(String keyName, String valueName, String value) throws IOException {
		
		openForWriting();
	    configProp.setProperty(keyName+valueName, value);	
	}
	

	public String readStringValue(String keyName, String valueName) throws IOException {
		
		openForWriting();
		return configProp.getProperty(keyName+valueName);
	}
	

	public String readStringValue(String keyName, String valueName, String defaultValue) throws IOException {
		
		openForWriting();
		return configProp.getProperty(keyName+valueName, defaultValue);
	}

	
	public void saveFrame(JFrame frame) throws IOException {
		
		openForWriting();
		String key=frame.getName()+".";
		writeIntValue(key,"left",frame.getX());
		writeIntValue(key,"top",frame.getY());
		writeIntValue(key,"width",frame.getWidth());
		writeIntValue(key,"height",frame.getHeight());
	}
	

	public void loadFrame(JFrame frame) throws IOException, ConfigException {
		
		int left,top,width,height;
		String key=frame.getName()+".";
		openForReading();
		left=readIntValue(key,"left");
		top=readIntValue(key,"top");
	    width=readIntValue(key,"width");
	    height=readIntValue(key,"height");
		frame.setBounds(left, top, width, height);
	}
	
}
