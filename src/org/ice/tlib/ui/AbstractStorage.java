package org.ice.tlib.ui;

import org.ice.tlib.db.DataBaseException;


public abstract class AbstractStorage extends Object {

	
	public abstract Object getCell(int aRowIndex, int aColIndex);

	
	public abstract Class<?> getColumnClass(int aColIndex) throws DataBaseException;

	
	public abstract String getTitle(int aColIndex);
	
	
	public abstract int getTotalColumns();

	
	public abstract void setTotalColumns(int totalColumns);

	
	public abstract int getTotalRows();

	
	public abstract void setTotalRows(int totalRows);
	
	
	public abstract int getVisibleColumns();
	
}
