package org.ice.tlib.ui;

public class Const {
//	public static final int CT_INT=0;
//	public static final int CT_STRING=1;
//	public static final int CT_DATE=2;
//	public static final int CT_UNDEF=-1;
	
	public static final int CT_INVALID=0;
	public static final int CT_VALID=1;
	
	public static final int CT_CLOSED=0;
	public static final int CT_OPENED=1;
}
