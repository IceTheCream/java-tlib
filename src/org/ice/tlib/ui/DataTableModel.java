package org.ice.tlib.ui;

import java.util.HashSet;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import org.ice.tlib.db.DataBaseException;

public class DataTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	protected AbstractStorage dataStorage;
	
	
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

	//***** ����, �����������
	public DataTableModel(AbstractStorage abstractStorage) {

		dataStorage=abstractStorage;
	}


	//***** ���������� ����� ������� ������� 
	public Class<?> getColumnClass(int aColumnIndex) {
		
		try {
		
			return dataStorage.getColumnClass(aColumnIndex);
		} catch (DataBaseException e) {
			
			System.out.println("\n *** Err. DataTableModel:getColumnClass:Data.getColumnClass - Exception occured. "+e.getMessage());
		}
		return null;
	}

	
	//***** ���������� ���������� �������� � ������� 
	public int getColumnCount() {

		return dataStorage.getVisibleColumns();
	}
	

	//***** ���������� �������� ������� 
	public String getColumnName(int colIndex) {

		if(colIndex<=dataStorage.getVisibleColumns()) {

			return dataStorage.getTitle(colIndex);
		} else {

			return "###";
		}
    }

	
	//***** ���������� ���������� ����� � ������� 
	public int getRowCount() {

		return dataStorage.getTotalRows();
	}

	
	//***** ���������� �������� ������ ������� 
	public Object getValueAt(int aRowIndex, int aColIndex) {

		return dataStorage.getCell(aRowIndex, aColIndex);
	}

	
	//***** ���������� ����������� �������������� ������ ������� [*] 
	public boolean isCellEditable(int aRowIndex, int aColIndex) {
	
		return false;
	}
		
	
	public void addTableModelListener(TableModelListener aListener) {

		listeners.add(aListener);
	}

	
	public void removeTableModelListener(TableModelListener aListener) {

		listeners.remove(aListener);
	}	
	
}
